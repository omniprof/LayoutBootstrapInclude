package com.kfwebstandard.selector;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class acts as a selector for a page selector There is no logic, it is
 * just a demonstration
 *
 * @author Ken
 */
@Named
@RequestScoped
public class Selector {

    private final static Logger LOG = LoggerFactory.getLogger(Selector.class);

    private final String select1;
    private final String select2;

    public Selector() {
        select1 = "/WEB-INF/content/content1.xhtml";
        select2 = "/WEB-INF/content/content2.xhtml";
    }

    public String getSelect1() {
        return select1;
    }

    public String getSelect2() {
        return select2;
    }

}
